# Jest / Puppeteer Proof of Concept

A POC of an SEO testing framework done for Halfords.com, using Jest and Puppeteer.

### Setup

Clone/download repo, navigate to folder, open Terminal and run `npm install`.

### To run tests

Open Terminal and run `npm test` command in root folder.
