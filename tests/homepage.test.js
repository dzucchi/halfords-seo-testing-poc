describe("Halfords home page", () => {
  beforeAll(async () => {
    jest.setTimeout(10000)
    await browser.newPage()
    await page.goto("https://www.halfords.com", { waitUntil: "load" })
  })

  it('Should display "Halfords" text on page', async () => {
    await expect(page).toMatch("Halfords")
  })

  it("Should match correct URL", async () => {
    const url = await page.url()

    expect(url).toBe("https://www.halfords.com/")
  })

  it("Should have meta description tag", async () => {
    const data = await page.evaluate(() => document.head.innerHTML)

    expect(data).toContain('<meta name="description"')
  })

  it("Should match title description", async () => {
    const title = await page.title()

    expect(title).toMatch(
      "Halfords - Bikes, Cycling, Camping, Car Parts, Sat Navs and More"
    )
  })

  it("When user clicks on Interest Free Credit Link, then navigates new page URL matches", async () => {
    await page.click("h5", { text: "12 Months" })

    const url = await page.url()

    expect(url).toMatch(
      "https://www.halfords.com/advice/cycling/buyers-guides/flexible-finance-at-halfords"
    )
  })
})
