describe("Cycling page", () => {
  beforeAll(async () => {
    jest.setTimeout(10000)
    await browser.newPage()
    await page.goto("https://www.halfords.com/cycling", { waitUntil: "load" })
  })

  it('Should display "Halfords" text on page', async () => {
    await expect(page).toMatch("Cycling")
  })

  it("Should match correct URL", async () => {
    const url = await page.url()

    expect(url).toBe("https://www.halfords.com/cycling")
  })

  it("When page opens, the it should have only one H1 header", async () => {
    const textContent = await page.evaluate(() =>
      [...document.querySelectorAll("h1")].map(elem => elem.innerText)
    )

    expect(textContent.length).toBe(1)
  })

  it("When page opens, page title should match requirement", async () => {
    const textContent = await page.evaluate(() =>
      [...document.getElementsByTagName("title")].map(elem => elem.innerText)
    )

    expect(textContent).toEqual([
      "Cycling | UKs Largest Bike and Cycling Shop | Halfords"
    ])
  })
})
